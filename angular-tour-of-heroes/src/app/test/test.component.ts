import { Component, OnInit } from '@angular/core';
import { EstadoBr, TestService } from './test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  estados: EstadoBr[];

  constructor(
    private test: TestService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.test.getEstadosBr().subscribe(
      (dados: EstadoBr[]) => {
      this.estados = dados;
      console.log(dados);
    }
    );
    console.log(this.estados);
  }

}
