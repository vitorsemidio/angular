import { AlertModalService } from './../../shared/alert-modal.service';
import { CursosService } from './../cursos.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cursos-form',
  templateUrl: './cursos-form.component.html',
  styleUrls: ['./cursos-form.component.scss']
})
export class CursosFormComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private service: CursosService,
    private alert: AlertModalService,
    private local: Location
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      nome: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(35)]]
    });
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.form.value);
    if(this.form.valid) {
      this.service.create(this.form.value).subscribe(
        sucesso => {
          this.alert.showAlertSuccess('Curso criado com sucesso');
          console.log(sucesso);
          this.local.back();
        },
        error => {
          this.alert.showAlertDanger('Erro ao criar curso. Tente novamente mais tarde');
          console.error(error);
        },
        () => console.log('Submit ok')
      );
    }
  }

  onCancel() {
    this.submitted = false;
    this.form.reset();
    console.log('Cancelou');
  }

  hasError(field) {
    return this.form.get(field).errors;
  }

}
