import { Char } from '../classe/char';
export const PERSONAGENS: Char[] = [
  {id: 1, name: 'Elesis', classe: 'Justiceira', arma: 'Espada'},
  {id: 2, name: 'Lire', classe: 'Caçadora', arma: 'Besta'},
  {id: 3, name: 'Arme', classe: 'Arquimaga', arma: 'Cajado'},
  {id: 4, name: 'Lass', classe: 'Ninja', arma: 'Adagas'},
  {id: 5, name: 'Ryan', classe: 'Druida', arma: 'Machado'},
  {id: 6, name: 'Ronan', classe: 'Defensor', arma: 'Espada'},
];
