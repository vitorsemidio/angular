import { CursosService } from './cursos.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit, OnDestroy {

  id: string;
  inscricao: Subscription;
  cursos: any[];
  paginaAtual: number;
  inscricaoQueryParams: Subscription;

  constructor(
    private route: ActivatedRoute,
    private cursosService: CursosService,
    private router: Router
  ) {
   }

  ngOnInit() {
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        this.id = params['id'];
      }
    );

    this.cursos = this.cursosService.getCursos();

    this.inscricaoQueryParams = this.route.queryParams.subscribe(
      (queryParams: any) => {
        this.paginaAtual = queryParams['pagina'];
      }
    );
  }


  ngOnDestroy() {
    this.inscricao.unsubscribe();
    this.inscricaoQueryParams.unsubscribe();
  }

  proximaPagina() {
    // this.pagina++;
    this.router.navigate(
      ['/cursos'], {queryParams: {'pagina': ++this.paginaAtual}}
    );
  }
  paginaAnterior() {
    // this.pagina++;
    this.router.navigate(
      ['/cursos'], {queryParams: {'pagina': --this.paginaAtual}}
    );
  }


}
