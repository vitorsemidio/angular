import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Processo } from './processo';
import { PROCESSOS } from './bd-processo';

@Injectable({
  providedIn: 'root'
})
export class ProcessoService {

  constructor() { }

  getProcessos(): Observable<Processo[]> {
    return of(PROCESSOS);
  }
}
