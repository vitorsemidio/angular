import { AlertModalService } from './../../shared/alert-modal.service';
import { catchError } from 'rxjs/operators';
import { Observable, empty, Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { CursosService } from '../cursos.service';
import { Curso } from '../curso';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertModalComponent } from '../../shared/alert-modal/alert-modal.component';

@Component({
  selector: 'app-cursos-lista',
  templateUrl: './cursos-lista.component.html',
  styleUrls: ['./cursos-lista.component.scss'],
  preserveWhitespaces: true
})
export class CursosListaComponent implements OnInit {

  // cursos: Curso[];

  cursos$: Observable<Curso[]>;
  error$ = new Subject<Boolean>();
  bsModalRef: BsModalRef;

  constructor(
    private service: CursosService,
    private alertService: AlertModalService
  ) { }

  ngOnInit() {
    // this.service.list().subscribe(dados => this.cursos = dados);
    this.onRefresh();
  }

  onRefresh() {
    this.cursos$ = this.service.list()
    .pipe(
      catchError(error => {
        console.error(error);
        this.handleError();
        return empty();
      })
    );

    // this.service.list().subscribe(
    //   dados => {
    //     console.log('sucesso', dados);
    //   },
    //   error => {
    //     console.log('error', error);
    //   },
    //   () => {
    //     console.log('Observable completo');
    //   }
    // );
  }


  handleError() {
    this.alertService.showAlertDanger('Erro ao carregar cursos. Tente novamente mais tarde');
  }

}
