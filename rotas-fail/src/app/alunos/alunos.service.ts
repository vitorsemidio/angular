import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlunosService {

  private alunos: any[] = [
    {id: 1, nome: 'Eren', email: 'Eren@gmail.com'},
    {id: 2, nome: 'Mikasa', email: 'Mikasa@gmail.com'},
    {id: 3, nome: 'Armin', email: 'Armin@gmail.com'}
  ];

  getAlunos() {
    return this.alunos;
  }

  getAluno(id: number) {
    for (let i = 0; i < this.alunos.length; i++) {
      const aluno = this.alunos[i];
      // tslint:disable-next-line:triple-equals
      if (aluno.id == id) {
        return aluno;
      }
    }

    return null;
  }

  constructor() { }
}
