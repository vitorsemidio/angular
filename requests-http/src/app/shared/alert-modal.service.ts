import { AlertModalComponent } from './alert-modal/alert-modal.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Injectable } from '@angular/core';

export enum AlertTypes {
  DANGER = 'danger',
  SUCCESS = 'success'
}

@Injectable({
  providedIn: 'root'
})
export class AlertModalService {

  constructor(
    private modalService: BsModalService
  ) { }

  showAlert(messege: string, type: string, dismissTimeout?: number) {
    const bsModalRef: BsModalRef = this.modalService.show(AlertModalComponent);
    bsModalRef.content.type = type;
    bsModalRef.content.message = messege;

    if (dismissTimeout) {
      setTimeout (() => bsModalRef.hide(), dismissTimeout);
    }
  }

  showAlertDanger(messege) {
    this.showAlert(messege, AlertTypes.DANGER);
  }
  showAlertSuccess(messege) {
    this.showAlert(messege, AlertTypes.SUCCESS, 3000);
  }
}
