import { AuthService } from './../login/auth.service';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router, CanLoad } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Route } from '@angular/compiler/src/core';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): boolean | Observable<boolean> {
      return this.verificarAcesso();
    }

    private verificarAcesso() {
      if (this.authService.usuarioEstaAutenticado()) {
        return true;
      }

      this.router.navigate(['/login']);
      return false;
  }



  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    console.log('canLoad: Verificando...');
    return this.verificarAcesso();
  }
}
