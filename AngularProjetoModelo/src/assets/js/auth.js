var Oidc = window.Oidc,
    UserManager = Oidc.UserManager;

var redirectUrl = window.location.protocol + "//" + window.location.host + window.location.pathname.replace("auth.html", "");

var manager = new UserManager();

var handleAuthError = function(er) {
    if (er.message == "No matching state found in storage") {
        window.location = redirectUrl;
    }

    document.getElementById("waiting").style.display = "none";
    document.getElementById("error").style.display = "block";
    document.getElementById("error").innerHTML = "Não foi possível entrar na Aplicação no momento. Clique <a class='label label-primary' href='" + window.location.pathname.replace("auth.html", "") + "'>aqui</a> para tentar entrar novamente.";
    
    console.error(er.message);
};

manager.signinRedirectCallback().then(function (user) {
    if (user == null) {
        document.getElementById("waiting").style.display = "none";
        document.getElementById("error").style.display = "block";
        document.getElementById("error").innerHTML = "Usuário não autenticado. Clique <a class='label label-primary' href='/login'>aqui</a> para tentar entrar novamente.";
    }
    else {
        window.location = redirectUrl;
    }
}).catch(function (er) {
    handleAuthError(er);
});
