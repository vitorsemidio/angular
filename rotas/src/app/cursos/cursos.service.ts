import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  getCursos() {
    return [
      {id: 1, nome: 'Angular'},
      {id: 2, nome: 'Java'},
      {id: 3, nome: 'JavaScript'},
      {id: 4, nome: 'PHP'},
      {id: 5, nome: 'HTML'},
      {id: 6, nome: 'CSS'},
    ];
  }

  getCurso(id: number) {
    const cursos = this.getCursos();

    for (let i = 0; i < cursos.length; i++) {
      const curso = cursos[i];
      // tslint:disable-next-line:triple-equals
      if (curso.id == id) {
        // console.log(curso);
        return curso;
      }
    }
    return null;
  }

  constructor() { }
}
