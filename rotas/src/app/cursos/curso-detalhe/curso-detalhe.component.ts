import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CursosService } from '../cursos.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-curso-detalhe',
  templateUrl: './curso-detalhe.component.html',
  styleUrls: ['./curso-detalhe.component.scss']
})
export class CursoDetalheComponent implements OnInit {
  inscricao: Subscription;
  id: number;
  curso: any;
  quantidadeCursos: number;

  constructor(
    private route: ActivatedRoute,
    private cursosService: CursosService,
    private router: Router
  ) { }

  ngOnInit() {
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        this.id = params['id'];

        this.curso = this.cursosService.getCurso(this.id);

        if (this.curso == null) {
          this.router.navigate(['cursos/naoEncontrado']);
        }
      }
    );

    this.quantidadeCursos = this.cursosService.getCursos().length;
  }

  cursoAnterior() {
    this.router.navigate(
      [`/cursos/${--this.id}`]
    );
  }

  proximoCurso() {
    this.router.navigate(
      [`/cursos/${++this.id}`]
    );
  }

  voltarParaCursos() {
    this.router.navigate(['/cursos']);
  }
}
