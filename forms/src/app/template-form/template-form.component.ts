import { HttpClient } from '@angular/common/http';
import { Component, OnInit, AfterViewInit, EventEmitter } from '@angular/core';
import { ConsultaCepService } from '../shared/services/consulta-cep.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

export class FormularioModel {
  nome: string;
  email: string;
  confirmarEmail: string;
  endereco: EnderecoModel;
}

export class EnderecoModel {
  cep: string;
  numero: string;
  complemento: string;
  rua: string;
  bairro: string;
  cidade: string;
  estado: string;
}

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit, AfterViewInit {
  arrayFormulario: FormularioModel[] = [
    { nome: ' Agnes', email: ' Agnes@gmail.com', confirmarEmail: ' Agnes@gmail.com',
      endereco: {
        cep: '22050901', numero: '10', complemento: 'Complemento C',
        rua: 'Rua C', bairro: 'Bairro C', cidade: 'Cidade C', estado: 'Estado C'
      }
    },
    { nome: ' Julia', email: ' Julia@gmail.com', confirmarEmail: ' Julia@gmail.com',
      endereco: {
        cep: '20550900', numero: '10', complemento: 'Complemento E',
        rua: 'Rua E', bairro: 'Bairro E', cidade: 'Cidade E', estado: 'Estado E'
      }
    },
    { nome: ' Luisa', email: ' Luisa@gmail.com', confirmarEmail: ' Luisa@gmail.com',
      endereco: {
        cep: '22631910', numero: '10', complemento: 'Complemento D',
        rua: 'Rua D', bairro: 'Bairro D', cidade: 'Cidade D', estado: 'Estado D'
      }
    },
    { nome: 'Rafael', email: 'Rafael@gmail.com', confirmarEmail: 'Rafael@gmail.com',
      endereco: {
        cep: ' 22231901', numero: '10', complemento: 'Complemento B',
        rua: 'Rua B', bairro: 'Bairro B', cidade: 'Cidade B', estado: 'Estado B'
      }
    },
    { nome: 'Emidio', email: 'Emidio@gmail.com', confirmarEmail: 'Emidio@gmail.com',
      endereco: {
        cep: '20211351', numero: '10', complemento: 'Complemento A',
        rua: 'Rua A', bairro: 'Bairro A', cidade: 'Cidade A', estado: 'Estado A'
      }
    },
  ]

  botaoFormulario: any;
  formBuilder: FormBuilder;







  usuario: any = {
    nome: null,
    email: null
  };

  onSubmit(formulario) {
    // console.log(formulario);

    // form.value
    // console.log(this.usuario);

    // this.http.post('https://httpbin.org/post', JSON.stringify(formulario.value))
    //   .subscribe(dados => {
    //     console.log(dados);
    //     formulario.form.reset();
    //   });
  }

  constructor(
    private http: HttpClient,
    private cepService: ConsultaCepService
  ) { }

  ngOnInit() {
    // this.populaDadosForm();
  }

  verificaValidTouched(campo) {
    return !campo.valid && campo.touched;
  }

  testeAutoClick(campo) {
    console.log(campo);
    console.log(campo.controls);
    console.log(campo.controls['email']);
    // if (campo.touched) {
    //   console.log('campo foi clicado');
    // } else {
    //   console.log('campo nao foi clicado');
    // }
  }

  aplicaCssErro(campo) {
    return {
      'has-error': this.verificaValidTouched(campo),
      'has-feedback': this.verificaValidTouched(campo)
    };
  }

  // consultaCEP(cep, form) {
  //   // Nova variável "cep" somente com dígitos.
  //   cep = cep.replace(/\D/g, '');

  //   if (cep != null && cep !== '') {
  //     this.cepService.consultaCEP(cep)
  //     .subscribe(dados => this.populaDadosForm(dados, form));
  //   }
  // }

  populaDadosForm(dados, formulario) {
    formulario.form.patchValue({
      nome: dados.nome,
      email: dados.email,
      endereco: {
          rua: dados.endereco.rua,
          cep: dados.endereco.cep,
          complemento: dados.endereco.complemento,
          bairro: dados.endereco.bairro,
          cidade: dados.endereco.cidade,
          estado: dados.endereco.estado,
          numero: dados.endereco.numero,
        }
    });
    // console.log(formulario);
    this.testeAutoClick(formulario);
  }

  resetaDadosForm(formulario) {
    formulario.form.patchValue({
      endereco: {
        rua: null,
        complemento: null,
        bairro: null,
        cidade: null,
        estado: null
      }
    });
  }

  preencherTudo(formulario: FormGroup) {
    // console.log(formulario.controls);
    console.log(formulario.controls['nome']);
    // return formulario.controls['nome'].markAsTouched;
    // this.testeAutoClick(formulario.controls['nome']);
    // console.log(formulario.controls['nome']);
    // for (let i = 0; i < formulario.controls.length; i++) {
    //   console.log(console.log(formulario.controls[i]));
    // }
    // console.log(formulario.controls['nome']);
  }

  verificaValidacoesForm(formGroup: FormGroup | FormArray) {
    Object.keys(formGroup.controls).forEach(campo => {
      console.log(campo);
      // const controle = formGroup.get(campo);
      // controle.markAsDirty();
      // controle.markAsTouched();
      // if (controle instanceof FormGroup || controle instanceof FormArray) {
      //   this.verificaValidacoesForm(controle);
      // }
    });
  }

  imprime(a) {
    console.log(a.form);
  }

  ngAfterViewInit() {
    // let x = document.getElementsByTagName('teste');
    // let x = document.getElementById('get');
    // x.addEventListener('click', this.populaDadosForm)
    // console.log(x);
    // console.log(x.onclick);
    // let evento = new EventEmitter();
    // evento.emit('click');
    // console.log(evento.emit('click'));
    // x.oncli;
  }

}
