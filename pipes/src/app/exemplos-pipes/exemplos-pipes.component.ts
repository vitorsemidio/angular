import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

@Component({
  selector: 'app-exemplos-pipes',
  templateUrl: './exemplos-pipes.component.html',
  styleUrls: ['./exemplos-pipes.component.css']
})
export class ExemplosPipesComponent implements OnInit {

  constructor() { }

  livro: any = {
    tipo: 'Manga',
    titulo: 'Shingeki no Kyojin',
    volume: '1',
    nota: 9.8513,
    capitulos: 4,
    preco: 50.00,
    dataLancamento: new Date(2013, 10, 23)
  };

  mangas: string[] = ['Shingeki no Kyojin', 'Bleach'];

  filtro: string;

  valorAsync = new Promise((resolve, reject) => {
    setTimeout(() => resolve('Valor assíncrono'), 2000);
  });


  // Programacao reativa
  // valorAsync2 = Observable.interval(2000).map(valor => 'Valor assíncrono 2');
  // teste = Observable.interval(2000).map(valor => 'Valor assíncrono 2');


  addManga(valor) {
    this.mangas.push(valor);
  }

  obterMangas() {
    if (this.mangas.length === 0 || this.filtro === undefined || this.filtro.trim() === '') {
      return this.mangas;
    }

    return this.mangas.filter((v) => {
      if (v.toLowerCase().indexOf(this.filtro.toLowerCase()) >= 0) {
        return true;
      }
      return false;
    });
  }

  ngOnInit() {
  }

}
