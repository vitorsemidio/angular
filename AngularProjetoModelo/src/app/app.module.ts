import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AppRoutingModule } from './app.routing.module';
import { SharedModule } from './shared/shared.module';
import { ApiService } from './services/api.service';
import { AuthService } from './services/auth.service';
import { ImagensService } from './services/imagens.service';
import { ResponseHandlerService } from './services/response-handler.service';
import { AuthorizationGuard } from './guards/authorizationGuard';
import { OcrComponent } from './ocr/ocr.component';
import { ProcessosComponent } from './processos/processos.component';
import { FormsModule } from '@angular/forms';
import { ProcessoDetalheComponent } from './processo-detalhe/processo-detalhe.component';
import { ModalComponent } from './modal/modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    OcrComponent,
    ProcessosComponent,
    ProcessoDetalheComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    AppRoutingModule,
    SharedModule,
    FormsModule,
    ModalModule.forRoot(),
  ],
  entryComponents: [
    ProcessoDetalheComponent
  ],
  providers: [
    ApiService,
    AuthService,
    AuthorizationGuard,
    ImagensService,
    ResponseHandlerService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
