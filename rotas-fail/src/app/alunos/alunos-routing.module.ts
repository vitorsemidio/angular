import { AlunosDeactivateGuard } from './../guards/aluno-deactivate.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { AlunosComponent } from './alunos.component';
import { AlunosGuard } from '../guards/alunos.guards';

const alunoRoutes: Routes = [
  {path: '', component: AlunosComponent, canActivateChild: [AlunosGuard], canDeactivate: [AlunosDeactivateGuard],
    children: [
    {path: 'novo', component: AlunoFormComponent},
    {path: ':id', component: AlunoDetalheComponent},
    {path: ':id/editar', component: AlunoFormComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(alunoRoutes)],
  exports: [RouterModule]
})
export class AlunosRoutingModule { }
