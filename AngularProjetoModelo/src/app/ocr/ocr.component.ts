import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Processo } from '../home/processo';
import { ProcessoDetalheComponent } from '../processo-detalhe/processo-detalhe.component';

@Component({
  selector: 'app-ocr',
  templateUrl: './ocr.component.html',
  styleUrls: ['./ocr.component.scss']
})
export class OcrComponent implements OnInit {

  siteName = 'ocr';

  constructor(
    private modalService: BsModalService
  ) { }


  alterarThread() {
    console.log('Quantidade de threads alterada com sucesso');
  }

  Exibir(processo: Processo): void {
    this.modalService.show(ProcessoDetalheComponent);
  }
  ngOnInit() {
  }

}
