import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { DataGridConfig, EnumAutoFitMode, EnumDataGridMode, InputFormsConfig, NgxUiHeroDataGridModule, NgxUiHeroInputFormsModule, NgxUiHeroModule } from 'ngx-ui-hero';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

defineLocale('pt-br', ptBrLocale);

const dataGridSettings: DataGridConfig = {
    emptyResultsMessage: 'Nenhum registro encontrado no momento.',
    infoMessage: 'Exibindo de {recordsFrom} até {recordsTo}, de {totalRecords} registros encontrados.',
    actionsColumnCaption: 'Ações',
    autoFitMode: EnumAutoFitMode.ByContent,
    mode: EnumDataGridMode.OnClient,
    paging: {
      firstText: '<<',
      previousText: '<',
      nextText: '>',
      lastText: '>>',
      boundaryLinks: true,
      directionLinks: true,
      rotate: true,
      maxSize: 10,
      itemsPerPage: 10
    },
    styles: {
      striped: true,
      bordered: true,
      hoverEffect: true,
      responsive: true
    },
    exporting: {
        exportButtonLabel: 'Exportar'
    }
};

const inputFormsConfig: InputFormsConfig = {
    currency: {
      currencyCode: 'BRL',
      align: 'right',
      allowNegative: true,
      allowZero: true,
      decimal: ',',
      thousands: '.',
      precision: 2,
      prefix: '',
      suffix: ''
    },
    validationMessages: {
      invalid: '{label} está inválido',
      required: '{label} é obrigatório',
      pattern: '{label} está inválido',
      maxlength: 'O valor preenchido é maior do que o máximo permitido',
      minlength: 'O valor preenchido é menor do que o mínimo permitido'
    },
    date: {
      format: 'dd/MM/yyyy',
      theme: 'theme-dark-blue',
      placement: 'bottom',
      locale: 'pt-br'
    },
    upload: {
      placeholder: 'Selecione um arquivo...',
      dropZonePlaceholder: 'Arraste e solte um arquivo aqui para importar.',
      autoUpload: true,
      showDropZone: true,
      showQueue: true,
      withCredentials: false,
      chunk: false,
      chunkSize: 1048576,
      chunkRetries: 3,
      maxFileSize: 4,
      selectButtonIcon: 'fa fa-folder',
      selectButtonLabel: 'Selecionar',
      removeButtonIcon: 'fa fa-trash',
      removeButtonLabel: 'Remover',
      fileTypeErrorMessage: 'A extensão [{extension}] não é permitida.',
      fileSizeErrorMessage: 'Este arquivo excede o tamanho máximo permitido de {maxFileSize}MB.',
      maxFileSizeLabel: 'Tamanho máximo permitido:',
      allowedExtensionsLabel: 'Extensões permitidas:'
    },
};

@NgModule({
    imports: [
        CommonModule,
        NgxUiHeroModule,
        NgxUiHeroDataGridModule.forRoot(dataGridSettings),
        NgxUiHeroInputFormsModule.forRoot(inputFormsConfig)
    ],
    declarations: [],
    providers: [],
    exports: [
        NgxUiHeroModule,
        NgxUiHeroDataGridModule,
        NgxUiHeroInputFormsModule
    ],
})
export class SharedModule { }
