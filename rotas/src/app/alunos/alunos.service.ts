import { Injectable } from '@angular/core';
import { Aluno } from './alunos';


@Injectable({
  providedIn: 'root'
})
export class AlunosService {

  private alunos: Aluno[] = [
    {id: 1, nome: 'Eren', email: 'eren@mail.com'},
    {id: 2, nome: 'Mikasa', email: 'mikasa@mail.com'},
    {id: 3, nome: 'Armin', email: 'armin@mail.com'},
    {id: 4, nome: 'Annie', email: 'annie@mail.com'},
    {id: 5, nome: 'Levi', email: 'levi@mail.com'},
  ];

  getAlunos() {
    return this.alunos;
  }

  getAluno(id: number) {
    for (let i = 0; i < this.alunos.length; i++) {
      const aluno = this.alunos[i];
      // tslint:disable-next-line:triple-equals
      if (id == aluno.id) {
        return aluno;
      }
    }

    return null;
  }

  constructor() { }
}
