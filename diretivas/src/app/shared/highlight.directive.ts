import { Directive, HostListener, ElementRef, Renderer, HostBinding,
Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor() { }

  @Input() defaultColor: string = 'white';
  @Input() highlightColor: string = 'yellow';

  @HostBinding('style.backgroundColor') varbgColor: string;

  @HostListener('mouseenter') onMouseOver() {
    this.varbgColor = this.highlightColor;
  }

  @HostListener('mouseleave') onMouseLeavever() {
    this.varbgColor = this.defaultColor;
  }

  ngOnInit(): void {
    this.varbgColor = this.defaultColor;
  }

}
