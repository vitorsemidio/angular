import { Router } from '@angular/router';
import { Injectable, EventEmitter } from '@angular/core';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private usuarioAutenticado = false;

  mostrarMenuEmitter = new EventEmitter<boolean>();
  public getMostrarMenuEmitter() {
    return this.mostrarMenuEmitter;
  }
  public setmostrarMenuEmitter(value) {
    this.mostrarMenuEmitter = value;
  }

  constructor(
    private router: Router
  ) { }

  fazerLogin(usuario: Usuario) {
    if (usuario.nome === 'teste@gmail.com' && usuario.senha === '1234') {
      this.usuarioAutenticado = true;
      this.router.navigate(['/']);
      this.mostrarMenuEmitter.emit(true);

    } else {
      this.usuarioAutenticado = false;
      this.mostrarMenuEmitter.emit(false);
    }
  }

  usuarioAuth() {
    return this.usuarioAutenticado;
  }
}
