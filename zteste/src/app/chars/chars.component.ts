import { Component, OnInit } from '@angular/core';

import { Char } from './../classe/char';
import { PERSONAGENS } from './../banco/bd-char';

@Component({
  selector: 'app-chars',
  templateUrl: './chars.component.html',
  styleUrls: ['./chars.component.css']
})
export class CharsComponent implements OnInit {

  // personagens: Char[];

  personagens = PERSONAGENS;
  hovered: number;

  constructor() { }

  ngOnInit() {
  }

}
