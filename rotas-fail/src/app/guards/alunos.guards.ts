import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class AlunosGuard implements CanActivateChild {
  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    if (state.url.includes('editar')) {
      // alert('Sem acesso');
      // console.log('Acesso not found');
      // return Observable.of(false);
      // return false;
      // return Observable.of(false);
      // return false;
    }

    return true;
  }
}
