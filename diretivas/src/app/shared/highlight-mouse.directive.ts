import { Directive, HostListener, ElementRef, Renderer, HostBinding } from '@angular/core';

@Directive({
  selector: '[appHighlightMouse]'
})
export class HighlightMouseDirective {

  constructor(
    //private _elementRef: ElementRef,
    //private _renderer: Renderer
  ) { }

  //@HostBinding('style.backgroundColor') varbgColor: string;
  private varbgColor: string;
  @HostBinding('style.backgroundColor') get setColor() {
    return this.varbgColor;
  }

  @HostListener('mouseenter') onMouseOver() {
    //this._renderer.setElementStyle(this._elementRef.nativeElement, 'background-color', 'yellow');
    this.varbgColor = 'yellow';
  }

  @HostListener('mouseleave') onMouseLeavever() {
    //this._renderer.setElementStyle(this._elementRef.nativeElement, 'background-color', 'white');
    this.varbgColor = 'white';
  }

}
