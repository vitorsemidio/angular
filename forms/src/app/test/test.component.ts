import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DropdownService } from '../shared/services/dropdown.service';

export interface Processo  {
  id: number;
  status: string;
  totalDePaginas: number;
  porcentagem: number;
  paginasConcluidas: number;
  posicao: number;
  nomeArquivo: string;
  dataDeCriacao: string;
  dataDeTermino?: string;
  codigoProcessamento: string;
  hash?: string;
}


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  x = {
    id: 4,
    status: 'TERMINADO',
    totalDePaginas: 7,
    porcentagem: 100,
    paginasConcluidas: 7,
    posicao: 0,
    nomeArquivo: 'TESSERACT_Teste com imagens incorporadas ao pdf e Texto nativo.pdf',
    dataDeCriacao: '2019-02-15 11:55:12',
    dataDeTermino: '2019-02-15 11:57:21',
    codigoProcessamento: '51201911545565596.07508',
    hash: 'f05be46402ff6007d98b0bc9fa379fa51bd46376'
  };
  // processos: Processo[];
  processos: any;
  info: any;
  xxx;

  constructor(
    private http: HttpClient,
    private servico: DropdownService
  ) { }

  ngOnInit() {
  }

  onGrid() {
    // this.servico.getEstadosBr().
    // console.log(this.http.get('assets/dados/dbocr.json'));
    // return this.http.get('assets/dados/dbocr.json')
    //   .subscribe(
    //     (dados: {ocr: Processo}) => {
    //       // console.log(dados);
    //       this.processos = dados.ocr;
    //     }
    //   );
    return this.http.get('http://cpu-7434:8080/lista-grid')
      .subscribe(
        (dados) => {
          console.log(dados);
          // this.info = dados;
          // this.processos = dados.ocr;
        }
      );
  }

  onQtdThreads() {
    return this.http.get('http://cpu-7434:8080/qtd-threads')
      .subscribe(
        (dados) => {
          console.log(dados);
          // this.info = dados;
          // this.processos = dados.ocr;
        }
      );
  }

  onPost() {
    return this.http.post('assets/dados/dbocr.json/ocr', this.x).subscribe();
    // return this.http.post('http://httpbin.org/post', this.x).subscribe();
    // return this.http.post('https://httpbin.org/post', JSON.stringify(this.x)).subscribe(
    //   dados => {
    //     console.log(dados);
    //   },
    //   error => console.log('Deu erro')
    // );
  }

  logProcessos() {
    console.log(this.processos);
  }

  setQtd() {
    return this.http.put('http://cpu-7434:8080/thread-config?qtdThreads=2', null)
      .subscribe(
        (dados) => {
          console.log(dados);
          // this.info = dados;
          // this.processos = dados.ocr;
        }
      );
  }

}
