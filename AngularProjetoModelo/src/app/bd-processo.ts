import { Processo } from './processo';

export const PROCESSOS: Processo[] = [
  {id: 1, name: 'Processo 1', status: 'red', pagina: 10 },
  {id: 2, name: 'Processo 2', status: 'yellow', pagina: 5 },
  {id: 3, name: 'Processo k', status: 'green', pagina: 8 },
  // {id: 4, name: 'Processo A', status: 'red', pagina: 19 },
  // {id: 5, name: 'Processo B', status: 'yellow', pagina: 15 },
  // {id: 6, name: 'Processo 80', status: 'green', pagina: 28 },
  // {id: 7, name: 'Processo 1A', status: 'red', pagina: 13 },
  // {id: 8, name: 'Processo 24', status: 'yellow', pagina: 35 },
  {id: 9, name: 'Processo 99', status: 'green', pagina: 84 },
  {id: 10, name: 'Processo XY', status: 'red', pagina: 111 },
  {id: 11, name: 'Processo XA', status: 'yellow', pagina: 25 },
  {id: 12, name: 'Processo UAAI', status: 'yellow', pagina: 35 },
  {id: 13, name: 'Processo AL', status: 'red', pagina: 13 },
  {id: 12, name: 'Processo UDI', status: 'red', pagina: 35 },
  {id: 13, name: 'Processo LA', status: 'green', pagina: 13 },
  {id: 12, name: 'Processo UI', status: 'green', pagina: 350 },
  {id: 13, name: 'Processo L55', status: 'green', pagina: 13 }
];
