import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface EstadoBr {
  Id: string;
  Sigla: string;
  Nome: string;
}

@Injectable({
  providedIn: 'root'
})
export class TestService {


  constructor(
    private http: HttpClient
  ) { }

  getEstadosBr() {
    console.log('GET');
    return this.http.get<EstadoBr[]>('assets/dados/estadosbr.json');
  }
}
