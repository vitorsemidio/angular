import { IFormCanDeactivate } from './../guards/iform-candeactivate';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlunosDeactivateGuard implements CanDeactivate<IFormCanDeactivate> {

  canDeactivate(
    component: IFormCanDeactivate,
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): boolean | Observable<boolean> {

      console.log('guarda de desativacao');
      // return component.podeMudarRota();

      return component.podeDesativar();
  }
}
