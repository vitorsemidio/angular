import { Injectable, EventEmitter } from '@angular/core';
import { Usuario } from './usuario.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private usuarioAutenticado = false;
  mostrarMenuEmitter = new EventEmitter<boolean>();

  constructor(
    private router: Router
  ) { }

  fazerLogin(usuario: Usuario) {
    // tslint:disable-next-line:triple-equals
    if (usuario.nome == 'usuario@gmail.com' && usuario.senha == '123') {

      this.usuarioAutenticado = true;
      this.mostrarMenuEmitter.emit(true);
      this.router.navigate(['/alunos']);

    } else {

      this.mostrarMenuEmitter.emit(false);
      this.usuarioAutenticado = false;
      this.router.navigate(['/login']);

    }
  }


  usuarioEstaAutenticado() {
    return this.usuarioAutenticado;
  }
}
