import { Component, OnInit } from '@angular/core';

import { MenuItemModel } from './menu-item.model';

declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  items: Array<MenuItemModel> = [
    {
      title: 'Painel de controle',
      icon: 'icons icon-speedometer',
      router: '/home',
      items: []
    },
  ];

  constructor() { }

  ngOnInit() {
    this.handleMenuCollapses();
  }

  private handleMenuCollapses(): void {
    setTimeout(() => {
      $('ul.menu-lateral li > span.menu-link').on('click', function(e){
        $(this).parent()
          .siblings()
          .children('.menu-link')
            .children('i.fa:last-child')
            .toggleClass('fa-angle-down', true)
            .toggleClass('fa-angle-up', false)
          .parent()
          .parent('li')
          .removeClass('open')
          .children('ul.submenu')
          .stop(true, true)
          .slideUp('fast');

        $(this)
          .children('i.fa:last-child')
            .toggleClass('fa-angle-down', $(this).parent().hasClass('open'))
            .toggleClass('fa-angle-up', !$(this).parent().hasClass('open'))
          .parent()
          .parent('li')
          .toggleClass('open')
          .children('ul.submenu')
          .stop(true, true)
          .slideToggle('fast');
      });
    }, 0);
  }

}
