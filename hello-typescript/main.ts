var anime = "Shingeki no Kyojin";
console.log("http://es6-features.org/#Constants");
console.log("tsc main.ts");
console.log("https://babeljs.io/");

function soma(x, y) {
    return x + y;
}

let num = 4;
const PI = 3.14;

var numeros = [1,2,3,4]

numeros.map(x => x*2); //ES 2015


class Matematica {
    soma(x, y) {
        return x+y;
    }
}


var n1: string = "teste";
//n1 = 4; //Daria erro pq a variavel foi declarada como tipo string

var n2: any = "teste"; //Para não definir o tipo, pode-se colocar any ou nada
n2 = 3;