import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { EstadoBr } from './../shared/models/estado-br.model';
import { DropdownService } from './../shared/services/dropdown.service';
import { ConsultaCepService } from '../shared/services/consulta-cep.service';
import { Observable, empty } from 'rxjs';
import { FormValidations } from '../shared/form-validations';
import { VerificaEmailService } from './services/verifica-email.service';
import { map, tap, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { BaseFormComponent } from '../shared/base-form/base-form.component';
import { Cidade } from '../shared/models/cidade';

export class FormularioModel {
  nome: string;
  email: string;
  confirmarEmail: string;
  endereco: EnderecoModel;
}

export class EnderecoModel {
  cep: string;
  numero: string;
  complemento: string;
  rua: string;
  bairro: string;
  cidade: string;
  estado: string;
}


@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: ['./data-form.component.css']
})
export class DataFormComponent extends BaseFormComponent implements OnInit {
  arrayFormulario: FormularioModel[] = [
    { nome: ' Agnes', email: ' Agnes@gmail.com', confirmarEmail: ' Agnes@gmail.com',
      endereco: {
        cep: '22050901', numero: '10', complemento: 'Complemento C',
        rua: 'Rua C', bairro: 'Bairro C', cidade: 'Cidade C', estado: 'Estado C'
      }
    },
    { nome: ' Julia', email: ' Julia@gmail.com', confirmarEmail: ' Julia@gmail.com',
      endereco: {
        cep: '20550900', numero: '10', complemento: 'Complemento E',
        rua: 'Rua E', bairro: 'Bairro E', cidade: 'Cidade E', estado: 'Estado E'
      }
    },
    { nome: ' Luisa', email: ' Luisa@gmail.com', confirmarEmail: ' Luisa@gmail.com',
      endereco: {
        cep: '22631910', numero: '10', complemento: 'Complemento D',
        rua: 'Rua D', bairro: 'Bairro D', cidade: 'Cidade D', estado: 'Estado D'
      }
    },
    { nome: 'Rafael', email: 'Rafael@gmail.com', confirmarEmail: 'Rafael@gmail.com',
      endereco: {
        cep: ' 22231901', numero: '10', complemento: 'Complemento B',
        rua: 'Rua B', bairro: 'Bairro B', cidade: 'Cidade B', estado: 'Estado B'
      }
    },
    { nome: 'Emidio', email: 'Emidio@gmail.com', confirmarEmail: 'Emidio@gmail.com',
      endereco: {
        cep: '20211351', numero: '10', complemento: 'Complemento A',
        rua: 'Rua A', bairro: 'Bairro A', cidade: 'Cidade A', estado: 'Estado A'
      }
    },
  ]

  // formulario: FormGroup;
  // arrayFormGroup: Array<FormGroup>;
  estados: EstadoBr[];
  cidades: Cidade[];
  // estados: Observable<EstadoBr[]>;
  cargos: any[];
  tecnologias: any[];
  newsletterOp: any[];

  frameworks = ['Angular', 'React', 'Vue', 'Sencha'];

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private dropdownService: DropdownService,
    private cepService: ConsultaCepService,
    private verificaEmailService: VerificaEmailService
  ) {
    super();
   }

  ngOnInit() {
    // console.log(this.formulario);

    this.dropdownService.getEstadosBr()
      .subscribe(dados => this.estados = dados);

    let a = this.arrayFormulario.map(v => {
      // this.formBuilder.group({
      //   nome: [null]
      // });
      v
      // console.log(v.nome);
    });
    // a = this.arrayFormulario;

    let b = new Array();
    for (let i = 0; i < this.arrayFormulario.length; i++) {
      let x = this.formBuilder.group({
        nome: [this.arrayFormulario[i].nome, [Validators.required, Validators.minLength(3), Validators.maxLength(35)]],
        email: [this.arrayFormulario[i].email, [Validators.required, Validators.email], [this.validarEmail.bind(this)]],
        confirmarEmail: [this.arrayFormulario[i].confirmarEmail, [FormValidations.equalsTo('email')]],

        endereco: this.formBuilder.group({
          cep: [this.arrayFormulario[i].endereco.cep, [Validators.required, FormValidations.cepValidator]],
          numero: [this.arrayFormulario[i].endereco.numero, Validators.required],
          complemento: [this.arrayFormulario[i].endereco.complemento],
          rua: [this.arrayFormulario[i].endereco.rua, Validators.required],
          bairro: [this.arrayFormulario[i].endereco.bairro, Validators.required],
          cidade: [this.arrayFormulario[i].endereco.cidade, Validators.required],
          estado: [this.arrayFormulario[i].endereco.estado, Validators.required]
        }),
      });
      b.push(x);
    }
    // console.log(b);

    // console.log(a);

    let x = this.formBuilder.group({
      nome: [this.arrayFormulario[0].nome, [Validators.required, Validators.minLength(3), Validators.maxLength(35)]],
      email: [this.arrayFormulario[0].email, [Validators.required, Validators.email], [this.validarEmail.bind(this)]],
      confirmarEmail: [null, [FormValidations.equalsTo('email')]],

      endereco: this.formBuilder.group({
        cep: [null, [Validators.required, FormValidations.cepValidator]],
        numero: [null, Validators.required],
        complemento: [null],
        rua: [null, Validators.required],
        bairro: [null, Validators.required],
        cidade: [null, Validators.required],
        estado: [null, Validators.required]
      }),
      autoPreencher: this.formBuilder.control(true)
    });
    // let x;
    // console.log(x);

    this.formulario = x;
    console.log(this.formulario);
    console.log(this.formulario.controls['nome'].value);
    // this.arrayFormulario.map(v => console.log(v));
    // this.defaultDados(this.arrayFormulario[0]);
    const values =  this.arrayFormulario;
    // console.log(values);
    // this.formulario = this.formBuilder.array(values);


      // console.log(this.arrayFormGroup.push(this.formulario));

  }

  buildFrameworks() {
    const values = this.frameworks.map(v => new FormControl(false));
    return this.formBuilder.array(values, FormValidations.requiredMinCheckbox(1));
  }
  buildAutoPreenchimento() {
    const values = this.arrayFormulario.map(v => new FormControl(false));
    return this.formBuilder.array(values);
  }

  submit() {
    console.log(this.formulario);

    let valueSubmit = Object.assign({}, this.formulario.value);

    valueSubmit = Object.assign(valueSubmit, {
      frameworks: valueSubmit.frameworks
      .map((v, i) => v ? this.frameworks[i] : null)
      .filter(v => v !== null)
    });

    console.log(valueSubmit);

    this.http
        .post('https://httpbin.org/post', JSON.stringify({}))
        .subscribe(
          dados => {
            console.log(dados);
          },
          (error: any) => alert('erro')
        );
  }

  // consultaCEP() {
  //   const cep = this.formulario.get('endereco.cep').value;

  //   if (cep != null && cep !== '') {
  //     this.cepService.consultaCEP(cep)
  //     .subscribe(dados => this.populaDadosForm(dados));
  //   }
  // }

  populaDadosForm(dados) {

    this.formulario.patchValue({
      endereco: {
        rua: dados.logradouro,
        complemento: dados.complemento,
        bairro: dados.bairro,
        cidade: dados.localidade,
        estado: dados.uf
      }
    });

    // this.formulario.get('nome').setValue('Loiane');
    // this.formulario.get('email').setValue('Loiane@gmail.com');
    // this.formulario.get('confirmarEmail').setValue('Loiane@gmail.com');
  }

  defaultDados(formularioDoArray: FormularioModel) {
    for(let i = 0; i < 5; i++) {

    }
    // this.formulario.get('nome').setValue(formularioDoArray.nome);
    // this.formulario.get('email').setValue(formularioDoArray.email);
    // this.formulario.get('confirmarEmail').setValue(formularioDoArray.confirmarEmail);
    // this.formulario.get('endereco.cep').setValue(formularioDoArray.endereco.cep);
    // this.formulario.get('endereco.numero').setValue(formularioDoArray.endereco.numero);
    // this.formulario.get('endereco.complemento').setValue(formularioDoArray.endereco.complemento);
    // this.formulario.get('endereco.rua').setValue(formularioDoArray.endereco.rua);
    // this.formulario.get('endereco.bairro').setValue(formularioDoArray.endereco.bairro);
    // this.formulario.get('endereco.estado').setValue(formularioDoArray.endereco.estado);
    // this.formulario.get('endereco.cidade').setValue(formularioDoArray.endereco.cidade);
  }

  resetaDadosForm() {
    this.formulario.patchValue({
      endereco: {
        rua: null,
        complemento: null,
        bairro: null,
        cidade: null,
        estado: null
      }
    });
  }

  setarCargo() {
    const cargo = { nome: 'Dev', nivel: 'Pleno', desc: 'Dev Pl' };
    this.formulario.get('cargo').setValue(cargo);
  }

  compararCargos(obj1, obj2) {
    return obj1 && obj2 ? (obj1.nome === obj2.nome && obj1.nivel === obj2.nivel) : obj1 === obj2;
  }

  setarTecnologias() {
    this.formulario.get('tecnologias').setValue(['java', 'javascript', 'php']);
  }

  validarEmail(formControl: FormControl) {
    return this.verificaEmailService.verificarEmail(formControl.value)
      .pipe(map(emailExiste => emailExiste ? { emailInvalido: true } : null));
  }
  teste() {
    this.formulario.get('email').setValue('Loiane@gmail.com');
  }
}
