import { Injectable } from '@angular/core';
import { Http, Headers, Request, RequestMethod, URLSearchParams, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { ResponseHandlerService } from './response-handler.service';
import { KeyValuePair } from '../models/key-value-pair';
import { AuthService } from './auth.service';

@Injectable()
export class ApiService {
  baseUrl: string = environment.webApiBaseUrl;

  constructor(
    private http: Http,
    private responseHandler: ResponseHandlerService,
    private authService: AuthService,
  ) {}

  get(url: string, query?: KeyValuePair[], headers?: Headers, fullUrl?: Boolean, responseType?: ResponseContentType, autoMapResponse?: boolean): Observable<any> {
    return this.sendRequest(
      this.buildUrl(url, fullUrl),
      RequestMethod.Get,
      query,
      null,
      headers,
      responseType,
      autoMapResponse
    );
  }
  post(url: string, data: any, headers?: Headers, fullUrl?: Boolean, responseType?: ResponseContentType, autoMapResponse?: boolean): Observable<any> {
    return this.sendRequest(
      this.buildUrl(url, fullUrl),
      RequestMethod.Post,
      null,
      data,
      headers,
      responseType,
      autoMapResponse
    );
  }
  put(url: string, data: any, headers?: Headers, fullUrl?: Boolean, responseType?: ResponseContentType, autoMapResponse?: boolean): Observable<any> {
    return this.sendRequest(
      this.buildUrl(url, fullUrl),
      RequestMethod.Put,
      null,
      data,
      headers,
      responseType,
      autoMapResponse
    );
  }
  delete(url: string, headers?: Headers, fullUrl?: Boolean, responseType?: ResponseContentType, autoMapResponse?: boolean): Observable<any> {
    return this.sendRequest(
      this.buildUrl(url, fullUrl),
      RequestMethod.Delete,
      null,
      null,
      headers,
      responseType,
      autoMapResponse
    );
  }

  private sendRequest(url: string , method: RequestMethod, query: KeyValuePair[], body: any, headers: Headers, responseType?: ResponseContentType, autoMapResponse?: boolean): Observable<any> {
    let requestBody: any = body;
    let responseContentType = ResponseContentType.Json;
    let canAutoMapResponse = true;

    if (responseType != null) {
      responseContentType = responseType;
    }

    if (autoMapResponse != null) {
      canAutoMapResponse = autoMapResponse;
    }

    if (body instanceof Object) {
      requestBody = this.getJson(body);
    }

    const request = new Request({
      method: method,
      url: url,
      params: this.getQueryStringParams(query),
      body: requestBody,
      headers: headers || this.buildRequestHeaders(),
      responseType: responseContentType
    });

    return this.http.request(request)
      .pipe(
        map(response => {
          if (canAutoMapResponse) {
            return this.responseHandler.handleSuccess(response, responseContentType);
          } else {
            return response;
          }        
        }),
        catchError(reason => this.responseHandler.handleError(reason))
      );
  }
  private getQueryStringParams(query: KeyValuePair[]): URLSearchParams {
    if (query == null) {
      return null;
    }

    const params: URLSearchParams = new URLSearchParams();

    query.forEach(x => {
      params.set(x.Key, x.Value);
    });

    return params;
  }
  private getJson(data: any): string {
    if (data == null) {
      return null;
    }
    return JSON.stringify(data);
  }
  private buildRequestHeaders(): Headers {
    const headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');

    if (this.authService.user) {
      const token = this.authService.getAuthorizationHeaderValue();
      headers.append('Authorization', token);
    }
    
    return headers;
  }
  private buildUrl(url: string, fullUrl: Boolean): string {
    if (!fullUrl) {
      return this.baseUrl + `${url}`;
    } else {
      return url;
    }    
  }
}
