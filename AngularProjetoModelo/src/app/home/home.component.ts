import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('situacao') classe;

  siteName = 'ocr';
  alertValue = '';
  corLetra = 'white';

  isShowed = true;
  isMouseStatus = false;

  qtdPagina = 10;

  constructor() {}

  showModel() {
    this.isShowed = true;
  }

  hideModel() {
    this.isShowed = false;
  }
  onMouseStatus() {
    const corAtual = this.classe.nativeElement.value;
    this.isMouseStatus = !this.isMouseStatus;
    if (this.isMouseStatus) {
      this.alertValue = 'alert ' +  corAtual;
      switch (this.classe.nativeElement.value) {
        case('alert-success'):
          this.corLetra = 'green';
          console.log(this.corLetra);
          break;

        case('alert-warning'):
          this.corLetra = 'yellow';
          console.log(this.corLetra);
          break;

        case('alert-danger'):
          this.corLetra = 'red';
          console.log(this.corLetra);
          break;
      }
    } else {
      this.alertValue = '';
      this.corLetra = 'white';
      console.log(this.corLetra);
    }


  }

  alterarThread() {
    alert('Quantidade de Threads alteradas com sucesso');
  }

  ngOnInit() {
  }

}
