import { BsModalService } from 'ngx-bootstrap';
import { Component, OnInit, ViewChild } from '@angular/core';

import { Processo } from '../processo';
import { ProcessoService } from '../processo.service';
import { ProcessoDetalheComponent } from '../processo-detalhe/processo-detalhe.component';

@Component({
  selector: 'app-processos',
  templateUrl: './processos.component.html',
  styleUrls: ['./processos.component.scss']
})
export class ProcessosComponent implements OnInit {

  @ViewChild('process') process;
  @ViewChild('testSpan') span;

  processos: Processo[];
  processoSelecionado: Processo;
  onProcesso: Processo;

  isMouseStatus = false;
  corLetra = 'white';
  color = 'red';


  hovered: number;
  unhovered: number;

  constructor(
    private processoService: ProcessoService,
    private modalService: BsModalService
    ) { }

  getProcessos(): void {
    this.processoService.getProcessos().subscribe(processos => this.processos = processos);
  }

  ngOnInit() {
    this.getProcessos();
  }

  onSelect(processo: Processo): void {
    this.processoSelecionado = processo;
    this.isMouseStatus = !this.isMouseStatus;
    this.corLetra = this.processoSelecionado.status;
  }

  unSelect(processo: Processo): void {
    this.processoSelecionado = null;
    this.isMouseStatus = !this.isMouseStatus;
  }

  onMouseStatus(processo: Processo): void {
    this.isMouseStatus = !this.isMouseStatus;
    this.onProcesso = processo;
    if (this.isMouseStatus) {

      console.log(processo.name);
      this.corLetra = processo.status;

    } else {
      this.corLetra = 'white';
    }
  }

  Exibir(processo: Processo): void {
    this.modalService.show(ProcessoDetalheComponent);
  }


  changeStyle($event) {
    this.color = $event.type === 'mouseover' ? 'yellow' : 'red';
  }



}
