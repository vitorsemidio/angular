var anime = "Shingeki no Kyojin";
console.log("http://es6-features.org/#Constants");
console.log("tsc main.ts");
console.log("https://babeljs.io/");
function soma(x, y) {
    return x + y;
}
var num = 4;
var PI = 3.14;
var numeros = [1, 2, 3, 4];
numeros.map(function (x) { return x * 2; }); //ES 2015
var Matematica = /** @class */ (function () {
    function Matematica() {
    }
    Matematica.prototype.soma = function (x, y) {
        return x + y;
    };
    return Matematica;
}());
var n1 = "teste";
//n1 = 4; //Daria erro pq a variavel foi declarada como tipo string
var n2 = "teste"; //Para não definir o tipo, pode-se colocar any ou nada
n2 = 3;
