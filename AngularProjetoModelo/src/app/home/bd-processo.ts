// import { Hero } from './hero';

// export const HEROES: Hero[] = [
//   { id: 11, name: 'Mr. Nice' },
//   { id: 12, name: 'Narco' },
//   { id: 13, name: 'Bombasto' },
//   { id: 14, name: 'Celeritas' },
//   { id: 15, name: 'Magneta' },
//   { id: 16, name: 'RubberMan' },
//   { id: 17, name: 'Dynama' },
//   { id: 18, name: 'Dr IQ' },
//   { id: 19, name: 'Magma' },
//   { id: 20, name: 'Tornado' }
// ];


import { Processo } from './processo';
export const PROCESSOS: Processo[] = [
  {id: 1, name: 'Processo 1', status: 'red', pagina: 10 },
  {id: 2, name: 'Processo 2', status: 'yellow', pagina: 5 },
  {id: 3, name: 'Processo k', status: 'green', pagina: 8 }
];
