import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlunosService } from '../alunos.service';
import { IFormCanDeactivate } from 'src/app/guards/iform-candeactivate';

@Component({
  selector: 'app-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements OnInit, OnDestroy, IFormCanDeactivate {

  inscricao: Subscription;
  aluno: any = {};
  formMudou = false;

  constructor(
    private route: ActivatedRoute,
    private alunosService: AlunosService,
    private router: Router) { }

    onInput() {
      this.formMudou = true;
      console.log('mudou');
    }

    podeMudarRota(): boolean {
      if (this.formMudou) {
        // confirm('Tem certeza que deseja sair da pagina?');
        return true;
      }
    }

    ngOnInit() {
      this.inscricao = this.route.params.subscribe(
        (params: any) => {
          const id = params['id'];

          this.aluno = this.alunosService.getAluno(id);

          if (this.aluno === null) {
            // this.router.navigate(['/alunos']);
            // console.log('nao existe');
            this.aluno = {};
          }
        }
      );
    }

    ngOnDestroy() {
      this.inscricao.unsubscribe();
    }

    podeDesativar() {
      return this.podeMudarRota();
    }

}
