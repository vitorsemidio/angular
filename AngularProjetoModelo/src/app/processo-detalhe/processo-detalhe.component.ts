import { Component, OnInit, Input } from '@angular/core';

import { Processo } from '../processo';


@Component({
  selector: 'app-processo-detalhe',
  templateUrl: './processo-detalhe.component.html',
  styleUrls: ['./processo-detalhe.component.scss']
})
export class ProcessoDetalheComponent implements OnInit {

  @Input() processo: Processo;
  // processo: Processo;

  constructor() { }

  unSelect(): void {
    this.processo = null;
    // this.processo = null;
  }

  ngOnInit() {
  }

}
