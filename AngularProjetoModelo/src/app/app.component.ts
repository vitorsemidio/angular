import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  sidebarCollapsed = true;
  
  OnToggleSidebar(): void {
    this.sidebarCollapsed = !this.sidebarCollapsed;
  }
}
