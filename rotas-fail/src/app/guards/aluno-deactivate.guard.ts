import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// import { AlunoFormComponent } from './../alunos/aluno-form/aluno-form.component';
import { AlunoFormComponent } from '../alunos/aluno-form/aluno-form.component';
import { CanDeactivate } from '@angular/router/src/utils/preactivation';
import { IFormCanDeactivate } from '../guards/iform-candeactivate';

@Injectable()
export class AlunosDeactivateGuard implements CanDeactivate<IFormCanDeactivate> {

    // constructor(private permissions: Permissions, private currentUser: UserToken) {}
    canDeactivate(
      component: IFormCanDeactivate,
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean>|Promise<boolean>|boolean {
      // return this.permissions.canDeactivate(this.currentUser, route.params.id);
      console.log('Desativacao da guarda');

      // component.formMudou
      // return !component.podeMudarRota;

      // return component.podeDesativar();
      // confirm('Deseja realmente sair?');
      // return false;
      console.log(component);
      return true;
      // return component.podeDesativar();

  }
}
