import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  host: {
    '(window:resize)': 'OnResize($event)'
  }
})
export class HeaderComponent implements OnInit {
  @Output() OnToggleSidebar = new EventEmitter();
  sidebarCollapsed = true;

  constructor() { }

  ngOnInit() {
    this.collapsarSideBarSeNecessario();
  }

  ToggleSideBar(): void {
    this.sidebarCollapsed = !this.sidebarCollapsed;
    this.OnToggleSidebar.emit();
  }

  OnResize(event){
    this.collapsarSideBarSeNecessario();
  }

  private collapsarSideBarSeNecessario(): void {
    if (window.innerWidth <= 900 && !this.sidebarCollapsed) {
      this.ToggleSideBar();
    }
  }

}
